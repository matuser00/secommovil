package gsecom.secommovil;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.iid.FirebaseInstanceId;
import org.json.JSONException;
import java.sql.Date;

public class InicioSesion extends AppCompatActivity implements BACKRequestListener {
    Button botonInicioSesion;
    EditText usuario;
    EditText contrasena;
    Button botonRegistrarse;
    ProgressDialog loading;
    TextView recuperarlink;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String idUsuario="";
        SharedPreferences prefs = getSharedPreferences(getString(R.string.nombresp), Context.MODE_PRIVATE);
        idUsuario=prefs.getString(getString(R.string.keyusuario),"");
        if(idUsuario!=""&&idUsuario!=null ) {
            //Dirigir a inicio de sesion:
            Intent fromDtoB = new Intent(this,MainActivity.class);
            fromDtoB.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(fromDtoB);
        }
        else{


            setContentView(R.layout.activity_inicio_sesion);
            usuario = (EditText) findViewById(R.id.iniciosesion_usuario);
            contrasena = (EditText) findViewById(R.id.iniciosesion_contrasena);
            botonInicioSesion = (Button) findViewById(R.id.iniciosesion_iniciarsesion);
            botonRegistrarse = (Button) findViewById(R.id.iniciosesion_registrarse);
            botonRegistrarse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intento = new Intent(InicioSesion.this, RegistroUsuario.class);
                    startActivity(intento);
                }
            });
            botonInicioSesion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Consultar el inicio de sesion
                    String strusuario = usuario.getText().toString().trim();
                    String strcontrasena = contrasena.getText().toString().trim();
                    if (strusuario.length() == 0 || strcontrasena.length() == 0) {
                        Toast.makeText(InicioSesion.this, "En codigo de usuario y la contraseña deben de estar llenos",
                                Toast.LENGTH_LONG).show();
                    } else {
                        //Iniciar sesion
                        loading = ProgressDialog.show(InicioSesion.this, "Iniciando sesion", "Espere un momento...", true, true);
                        BACKRequestJob requestInicioSesion = new BACKRequestJob(InicioSesion.this, getString(R.string.diriniciosesion));
                        requestInicioSesion.addBACKRequestListener(InicioSesion.this);
                        requestInicioSesion.metodo = BACKRequestJob.METODO_POST;
                        requestInicioSesion.execute("email=" + usuario.getText(), "password=" + contrasena.getText());
                    }

                }
            });
            recuperarlink = (TextView) findViewById(R.id.iniciosesion_recuperarcon);
            recuperarlink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intrecuperar = new Intent(InicioSesion.this, recuperarcontrasena.class);
                    startActivity(intrecuperar);
                }
            });
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onExito(BACKRequestEvent e) {
        loading.dismiss();
        if(e.respuestaJSON!=null) {
            SharedPreferences sharedPref =getApplicationContext().getSharedPreferences(getString(R.string.nombresp),Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            try {
                int codigo_scotiabank=e.respuestaJSON.getJSONObject("depto").getInt("codigo_scotiabank");
                editor.putInt(this.getString(R.string.codscotiabank),codigo_scotiabank);
                editor.commit();
                String codcliente=e.respuestaJSON.getJSONObject("depto").getString(this.getString(R.string.codcliente));
                editor.putString(this.getString(R.string.codcliente),codcliente);
                editor.commit();
                editor.putString(getString(R.string.keyusuario), e.respuestaJSON.getString(getString(R.string.campoidusuario)));
                editor.commit();
                BACKRequestRegistroToken.startRegistro(this.getApplicationContext(), e.respuestaJSON.getString(getString(R.string.campoidusuario)), FirebaseInstanceId.getInstance().getToken());

                Date fechaToken = new Date(FirebaseInstanceId.getInstance().getCreationTime());
                Intent intento = new Intent(this, MainActivity.class);
                String mensajenotificacion=this.getIntent().getStringExtra("mensajenotificacion");

                if(mensajenotificacion!=""&&mensajenotificacion!=null){
                    intento.putExtra("mensajenotificacion",mensajenotificacion);
                }

                startActivity(intento);
            } catch (JSONException e1) {
                e1.printStackTrace();
                Toast.makeText(InicioSesion.this, "Error al obtener los datos del usuario", Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(InicioSesion.this,"El email y/o la contraseña son incorrectos, intentalo de nuevo.",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onError(BACKRequestEvent e) {
        loading.dismiss();
        Toast.makeText(this,e.mensaje,Toast.LENGTH_LONG).show();
    }
}
