package gsecom.secommovil;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class BACKRequestRegistroToken extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_FOO = "gsecom.secommovil.action.FOO";
    private static final String ACTION_BAZ = "gsecom.secommovil.action.BAZ";

    // TODO: Rename parameters
    private static final String EXTRA_PARAM1 = "gsecom.secommovil.extra.PARAM1";
    private static final String EXTRA_PARAM2 = "gsecom.secommovil.extra.PARAM2";

    public BACKRequestRegistroToken() {
        super("BACKRequestRegistroToken");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startRegistro(Context context, String idusuario, String token) {
        if(idusuario!=null) {
            if (!idusuario.isEmpty() && !token.isEmpty()) {
                Intent intent = new Intent(context, BACKRequestRegistroToken.class);
                intent.putExtra("id_usuario", idusuario);
                intent.putExtra("token", token);
                context.startService(intent);
            }
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            String refreshedToken =intent.getExtras().getString("token");
            String idusuario=intent.getExtras().getString("id_usuario");
            HttpURLConnection conn = null;

            DataOutputStream dos = null;
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";
            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024 * 1024;

            String[] parametrosPOST = {"token=" + refreshedToken, "id_usuario=" + idusuario};
            try {
                StringBuilder parametrosCodificados = new StringBuilder();
                boolean primero = true;
                if (parametrosPOST.length > 0) {
                    for (String parametro : parametrosPOST) {
                        int lenparametro = parametro.trim().length();
                        if (lenparametro > 0) {
                            String[] propiedadValor = parametro.split("=");
                            if (primero)
                                primero = false;
                            else
                                parametrosCodificados.append("&");
                            parametrosCodificados.append(URLEncoder.encode(propiedadValor[0], "UTF-8"));
                            parametrosCodificados.append("=");
                            parametrosCodificados.append(URLEncoder.encode(propiedadValor[1], "UTF-8"));
                        }
                    }
                }
                String urlParameters = parametrosCodificados.toString();
                byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
                int postDataLength = postData.length;
                String request = this.getString(R.string.dirtoken);
                URL url = new URL(request);
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setInstanceFollowRedirects(false);
                conn.setRequestMethod("POST");

                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                if (parametrosPOST.length > 0) {
                    if (parametrosPOST[0] != "")
                        conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
                }
                conn.setUseCaches(false);

                if (postDataLength > 0) {
                    try (
                            DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                            wr.write(postData);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (200 <= conn.getResponseCode() && conn.getResponseCode() <= 299) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                    String line, newjson = "";
                    while ((line = reader.readLine()) != null) {
                        newjson += line;
                    }
                    String json = newjson.toString();
                    reader.close();
                } else {
                    Toast.makeText(getApplicationContext(),"Error al leer la respuesta del servidor.",Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
