package gsecom.secommovil;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;

public class RegistroUsuario extends AppCompatActivity implements BACKRequestListener {
    Button botonRegistrar;
    EditText nombre;
    EditText contrasena;
    EditText email;
    EditText telefono;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_usuario);
        nombre=(EditText)findViewById(R.id.registrousuario_nombre);
        contrasena=(EditText)findViewById(R.id.registrousuario_contrasena);
        email=(EditText)findViewById(R.id.registrousuario_email);
        telefono=(EditText)findViewById(R.id.registrousuario_confcontrasena);

        botonRegistrar =(Button)findViewById(R.id.registrousuario_registrar);
        botonRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str_nombre=nombre.getText().toString().trim();
                String str_contrasena=contrasena.getText().toString().trim();
                String str_email=email.getText().toString().trim();
                String str_confcontrasena=telefono.getText().toString().trim();
                if(str_nombre.isEmpty()||str_contrasena.isEmpty()||str_email.isEmpty()||str_confcontrasena.isEmpty()){
                    Toast.makeText(RegistroUsuario.this,"Todos los campos deben estar llenos.",Toast.LENGTH_LONG).show();
                }else{
                    //BACKRequestJob requestRegistrar=new BACKRequestJob(RegistroUsuario.this,getString(R.string.dirregistro));
                    BACKRequestJob requestRegistrar=new BACKRequestJob(RegistroUsuario.this,getString(R.string.dirregistro)+"?email="+str_email+"&codigo_cliente="+str_nombre+"&password="+str_contrasena);
                    requestRegistrar.metodo="POST";
                    requestRegistrar.addBACKRequestListener(RegistroUsuario.this);
                    if(str_contrasena.contentEquals(str_confcontrasena)&&isEmailValid(str_email)) {
                        requestRegistrar.execute("email=" + str_email, "codigo_cliente=" + str_nombre, "password=" + str_contrasena);
                        //requestRegistrar.execute("");
                    }else {
                        if(!isEmailValid(str_email))
                            Toast.makeText(RegistroUsuario.this, "El email esta mal formado", Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(RegistroUsuario.this, "Las contraseñas no coinciden", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    public static final boolean isEmailValid(String email) {
        return  android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    public void onExito(BACKRequestEvent e) {
        //TODO: Realizar flujo para iniciar sesión depsues de registrarse
        //Verificar el codigo de cliente:
        String mensaje;
        try {
            if(e.respuestaJSON.getString("msg")=="1")
                Toast.makeText(RegistroUsuario.this,getString(R.string.mensaje_codigoclientenoexiste),Toast.LENGTH_LONG).show();
            if(e.respuestaJSON.getString("msg")=="4")//Codigo de cliente y usuario ya corresponden a una cuenta activa
                Toast.makeText(RegistroUsuario.this,getString(R.string.mensaje_emailasociado),Toast.LENGTH_LONG).show();
            if(e.respuestaJSON.getString("msg")=="2")
                Toast.makeText(RegistroUsuario.this,getString(R.string.mensaje_cuentaactivada),Toast.LENGTH_LONG).show();
            if(e.respuestaJSON.getString("msg")=="3")
                Toast.makeText(RegistroUsuario.this,getString(R.string.mensaje_registroexitoso),Toast.LENGTH_LONG).show();
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public void onError(BACKRequestEvent e) {
        Toast.makeText(this, e.mensaje, Toast.LENGTH_SHORT).show();
    }
}
