package gsecom.secommovil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by tomas on 19/10/16.
 */

public class DTOPago {
    public int idPago;
    public String fechaPago;
    public String toString(){

        String tituloItemPago="Consumo: ";
        Locale esLocale = new Locale("es", "ES");//para trabajar en español
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//El formato con que llega mi strFecha más el lenguaje
        try {
            String[] splitFechaPago=fechaPago.split("-");
            Date dateFechaPago=formatter.parse(fechaPago+" 00:00:00");
            SimpleDateFormat formatter2= new SimpleDateFormat("MMMM-yyyy");
            tituloItemPago+= formatter2.format(dateFechaPago).toUpperCase();
            //tituloItemPago+= dateFechaPago.toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return tituloItemPago;
    }
}
