package gsecom.secommovil;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
//TODO: Pedir a Cesar el aviso de privacidad
public class LectorPdfWV extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lector_pdf_wv);
        ((WebView)findViewById(R.id.lectorpdf_webview)).setWebChromeClient(new WebChromeClient());
        ((WebView)findViewById(R.id.lectorpdf_webview)).loadUrl(getIntent().getStringExtra("url"));
    }
}