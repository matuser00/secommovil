package gsecom.secommovil;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import org.json.JSONException;

public class HistorialPagos extends Fragment implements BACKRequestListener, OnRefreshListener{
    ListView iListaHistorialPagos=null;
    View vista=null;
    BACKRequestJob requestHistorialPagos=null;
    String idUsuario="";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vista=inflater.inflate(R.layout.activity_historial_pagos,container,false);

        SharedPreferences prefs = getContext().getSharedPreferences(getString(R.string.nombresp),Context.MODE_PRIVATE);
        idUsuario=prefs.getString(getString(R.string.keyusuario),"");

        iListaHistorialPagos=((ListView)vista.findViewById(R.id.iListaHistorialPagos));
        requestHistorialPagos=new BACKRequestJob(getActivity(),getString(R.string.dirhistorialpagos)+idUsuario);
        requestHistorialPagos.addBACKRequestListener(this);
        requestHistorialPagos.execute("");
        ((Button)vista.findViewById(R.id.historialpagos_obdatos_reintentar)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((TextView)vista.findViewById(R.id.historialpagos_obdatos_texto)).setText("Obteniendo datos...");
                requestHistorialPagos.cancel(true);
                if(requestHistorialPagos.isCancelled()){
                    requestHistorialPagos=new BACKRequestJob(getActivity(),getString(R.string.dirhistorialpagos)+idUsuario);
                    requestHistorialPagos.addBACKRequestListener(HistorialPagos.this);
                    requestHistorialPagos.execute("");
                    getView().findViewById(R.id.pagos_sinPagos).setVisibility(View.GONE);
                    getView().findViewById(R.id.historialpagos_obdatos_reintentar).setVisibility(View.VISIBLE);
                }
            }
        });
        return vista;
    }

    @Override
    public void onExito(BACKRequestEvent e) {
        if(e.respuestaJSONArray.length()>0){
            ArrayAdapter<DTOPago> aaHistorialPagos=new ArrayAdapter<DTOPago>(getActivity().getApplicationContext(),R.layout.historiallistapagos_item,R.id.tvitemlistapagos);
            iListaHistorialPagos.setAdapter(aaHistorialPagos);
            for(int i=0;i<e.respuestaJSONArray.length();i++){
                try {
                    DTOPago pago= new DTOPago();
                    pago.fechaPago=e.respuestaJSONArray.getJSONObject(i).getString("fecha_pago");
                    pago.idPago=e.respuestaJSONArray.getJSONObject(i).getInt("id_pago");
                    aaHistorialPagos.add(pago);
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }
            iListaHistorialPagos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intento= new Intent(getActivity().getApplicationContext(),Pago.class);
                    intento.putExtra("pago",((DTOPago)parent.getItemAtPosition(position)).idPago);
                    startActivity(intento);
                }
            });
            aaHistorialPagos.notifyDataSetChanged();
            getView().findViewById(R.id.historialpagos_obteniendodatos).setVisibility(View.INVISIBLE);
            getView().findViewById(R.id.pagos_sinPagos).setVisibility(View.GONE);
            getView().findViewById(R.id.iListaHistorialPagos).setVisibility(View.VISIBLE);
        }else{
            getView().findViewById(R.id.pagos_sinPagos).setVisibility(View.VISIBLE);
            getView().findViewById(R.id.historialpagos_obteniendodatos).setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public void onError(BACKRequestEvent e) {
        ((TextView)getView().findViewById(R.id.historialpagos_obdatos_texto)).setText(e.mensaje);
        getView().findViewById(R.id.historialpagos_obdatos_reintentar).setVisibility(View.VISIBLE);
        getView().findViewById(R.id.pagos_sinPagos).setVisibility(View.GONE);
        getView().findViewById(R.id.historialpagos_obteniendodatos).setVisibility(View.VISIBLE);
    }

    @Override
    public void onRefresh() {
        ((TextView)vista.findViewById(R.id.historialpagos_obdatos_texto)).setText("Obteniendo datos...");
        vista.findViewById(R.id.historialpagos_obdatos_reintentar).setVisibility(View.INVISIBLE);
        vista.findViewById(R.id.historialpagos_obteniendodatos).setVisibility(View.VISIBLE);
        vista.findViewById(R.id.iListaHistorialPagos).setVisibility(View.GONE);
        getView().findViewById(R.id.pagos_sinPagos).setVisibility(View.GONE);
        requestHistorialPagos=new BACKRequestJob(getActivity(),getString(R.string.dirhistorialpagos)+idUsuario);
        requestHistorialPagos.addBACKRequestListener(HistorialPagos.this);
        requestHistorialPagos.execute("");
    }
}
