package gsecom.secommovil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by MAT17 on 15/12/2016.
 */

public class DTOEvento {
    public String texto;
    public String fecha;
    public String toString(){

        String tituloItemPago="Evento: ";
        Locale esLocale = new Locale("es", "ES");//para trabajar en español
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//El formato con que llega mi strFecha más el lenguaje
        try {
            String[] splitFechaPago=fecha.split("-");
            Date dateFechaPago=formatter.parse(fecha+" 00:00:00");
            SimpleDateFormat formatter2= new SimpleDateFormat("MMMM-yyyy");
            tituloItemPago+= formatter2.format(dateFechaPago).toUpperCase();
            tituloItemPago+=" - "+texto;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return tituloItemPago;
    }
}
