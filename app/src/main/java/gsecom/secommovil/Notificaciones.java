package gsecom.secommovil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.vision.text.Line;

import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


public class Notificaciones extends Activity implements BACKRequestListener,OnRefreshListener {

    ListView iListaHistorialNotificaciones =null;
    View vista=null;
    BACKRequestJob requestHistorialNotificaciones =null;
    String idUsuario="";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notificaciones);
        SharedPreferences prefs = getSharedPreferences(getString(R.string.nombresp), Context.MODE_PRIVATE);
        idUsuario=prefs.getString(getString(R.string.keyusuario),"");
        if(idUsuario!=""&&idUsuario!=null ) {
            iListaHistorialNotificaciones = ((ListView) findViewById(R.id.iListaHistorialNotificaciones));
            requestHistorialNotificaciones = new BACKRequestJob(Notificaciones.this, getString(R.string.dirnotificaciones) + idUsuario);
            requestHistorialNotificaciones.addBACKRequestListener(this);
            requestHistorialNotificaciones.execute("");
            ((Button) findViewById(R.id.notificaciones_obdatos_reintentar)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((TextView) findViewById(R.id.notificaciones_obdatos_texto)).setText("Obteniendo datos...");
                    requestHistorialNotificaciones.cancel(true);
                    if (requestHistorialNotificaciones.isCancelled()) {
                        requestHistorialNotificaciones = new BACKRequestJob(Notificaciones.this, getString(R.string.dirnotificaciones) + idUsuario);
                        requestHistorialNotificaciones.addBACKRequestListener(Notificaciones.this);
                        requestHistorialNotificaciones.execute("");
                        Notificaciones.this.findViewById(R.id.notificaciones_sinNotificaciones).setVisibility(View.GONE);
                        Notificaciones.this.findViewById(R.id.notificaciones_obdatos_reintentar).setVisibility(View.VISIBLE);
                    }
                }
            });
        }else{
            //Dirigir a inicio de sesion:
            Intent fromDtoB = new Intent(this,InicioSesion.class);
            fromDtoB.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(fromDtoB);
        }
    }

    @Override
    public void onExito(BACKRequestEvent e) {
        if(e.respuestaJSONArray.length()>0){
            ArrayList<DTONotificacion> listaNotificaciones=new ArrayList<DTONotificacion>();
            for(int i=0;i<e.respuestaJSONArray.length();i++){
                try {
                    DTONotificacion notificacion= new DTONotificacion();
                    notificacion.imagen=""+e.respuestaJSONArray.getJSONObject(i).getString("imagen");
                    notificacion.mensaje=""+e.respuestaJSONArray.getJSONObject(i).getString("mensaje");
                    notificacion.tipo=""+e.respuestaJSONArray.getJSONObject(i).getString("tipo");
                    notificacion.id_mensaje=""+e.respuestaJSONArray.getJSONObject(i).getString("id_mensaje");
                    listaNotificaciones.add(notificacion);
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }
            if(!listaNotificaciones.isEmpty()) {
                DTONotificacionesAdapter aaHistorialNotificaciones = new DTONotificacionesAdapter(Notificaciones.this, listaNotificaciones);
                Parcelable state=iListaHistorialNotificaciones.onSaveInstanceState();
                iListaHistorialNotificaciones.setAdapter(aaHistorialNotificaciones);
                iListaHistorialNotificaciones.onRestoreInstanceState(state);
                //((ListView) Notificaciones.this.findViewById(R.id.iListaHistorialNotificaciones)).setAdapter(aaHistorialNotificaciones);

                Notificaciones.this.findViewById(R.id.notificaciones_obteniendodatos).setVisibility(View.INVISIBLE);
                Notificaciones.this.findViewById(R.id.notificaciones_sinNotificaciones).setVisibility(View.GONE);
                Notificaciones.this.findViewById(R.id.iListaHistorialNotificaciones).setVisibility(View.VISIBLE);
            }else {
                (Notificaciones.this.findViewById(R.id.notificaciones_sinNotificaciones)).setVisibility(View.VISIBLE);
                Notificaciones.this.findViewById(R.id.notificaciones_obteniendodatos).setVisibility(View.INVISIBLE);
            }
        }else{
            Notificaciones.this.findViewById(R.id.notificaciones_sinNotificaciones).setVisibility(View.VISIBLE);
            Notificaciones.this.findViewById(R.id.notificaciones_obteniendodatos).setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public void onError(BACKRequestEvent e) {
        ((TextView)Notificaciones.this.findViewById(R.id.notificaciones_obdatos_texto)).setText(e.mensaje);
        Notificaciones.this.findViewById(R.id.notificaciones_obdatos_reintentar).setVisibility(View.VISIBLE);
        Notificaciones.this.findViewById(R.id.notificaciones_sinNotificaciones).setVisibility(View.GONE);
        Notificaciones.this.findViewById(R.id.notificaciones_obteniendodatos).setVisibility(View.VISIBLE);
    }

    @Override
    public void onRefresh() {
        ((TextView)findViewById(R.id.notificaciones_obdatos_texto)).setText("Obteniendo datos...");
        findViewById(R.id.notificaciones_obdatos_reintentar).setVisibility(View.INVISIBLE);
        findViewById(R.id.notificaciones_obteniendodatos).setVisibility(View.VISIBLE);
        findViewById(R.id.iListaHistorialNotificaciones).setVisibility(View.GONE);
        Notificaciones.this.findViewById(R.id.notificaciones_sinNotificaciones).setVisibility(View.GONE);
        requestHistorialNotificaciones =new BACKRequestJob(Notificaciones.this,getString(R.string.dirnotificaciones)+idUsuario);
        requestHistorialNotificaciones.addBACKRequestListener(Notificaciones.this);
        requestHistorialNotificaciones.execute("");
    }
    @Override
    public void onBackPressed() {
        Intent intentoLogin= new Intent(this,MainActivity.class);
        intentoLogin.putExtra("desdeactividad",true);
        intentoLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intentoLogin);
        return;
    }
}


//Adaptador especializados para notificaciones
class DTONotificacionesAdapter extends ArrayAdapter<DTONotificacion> {
    private final ArrayList<DTONotificacion> listaNotificaciones;
    private final Activity contexto;

    public DTONotificacionesAdapter(Activity context, ArrayList<DTONotificacion> listaNotificaciones) {
        super(context, 0,listaNotificaciones);
        this.contexto= context;
        this.listaNotificaciones=listaNotificaciones;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position

        ViewHolder holder=null;
        DTONotificacion notificacion = getItem(position);
        //Se maneja cache de vista:
        //if(convertView==null) {
            holder= new ViewHolder();
            LayoutInflater inflator = contexto.getLayoutInflater();
            /*convertView = inflator.inflate(R.layout.mensaje_gsecom, null);
            holder.mensaje=(TextView) convertView.findViewById(R.id.textomensajegsecom);*/
            if (notificacion.tipo.equals("1")) {//Enviado por usuario
                convertView = inflator.inflate(R.layout.mensaje_usuario, null);
                holder.mensaje=(TextView) convertView.findViewById(R.id.textomensajeusuario);
                holder.imagenMensaje=(ImageView) convertView.findViewById(R.id.imagenmensajeusuario);
            } else {//Desde GSECOM
                convertView = inflator.inflate(R.layout.mensaje_gsecom,null);
                holder.mensaje= (TextView) convertView.findViewById(R.id.textomensajegsecom);
                holder.imagenMensaje=null;
            }
            convertView.setTag(holder);
            convertView.setTag(R.id.textomensajegsecom, holder.mensaje);
        /*}else{
            holder = (ViewHolder) convertView.getTag();
        }*/
        //Se maneja el set de datos:
        if (notificacion.tipo.equals("1")) {//Enviado por usuario
            if (holder.mensaje != null) {
                if (!notificacion.mensaje.isEmpty()) {
                    holder.mensaje.setText(notificacion.mensaje);
                } else {
                    if (notificacion.imagen.isEmpty())
                        holder.mensaje.setVisibility(View.GONE);
                    else
                        holder.mensaje.setText("-------------");
                }
            }
            if (holder.imagenMensaje != null) {
                if (!notificacion.imagen.isEmpty()) {
                    String dirback = getContext().getString(R.string.dirbaseback);
                    new DownloadImageTask(holder.imagenMensaje)
                            .execute(dirback + notificacion.imagen);
                    holder.imagenMensaje.setVisibility(View.VISIBLE);
                } else {
                    holder.imagenMensaje.setVisibility(View.GONE);
                }
            }
        } else {//Desde GSECOM
            if (holder.mensaje != null) {
                SharedPreferences prefs = this.getContext().getSharedPreferences(this.getContext().getString(R.string.nombresp), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                String mensajes_leidos = prefs.getString("mensajes_leidos", "");
                mensajes_leidos = mensajes_leidos != null ? mensajes_leidos : "";
                holder.mensaje.setText(notificacion.mensaje);
                if (mensajes_leidos.indexOf(notificacion.id_mensaje + ",") == -1) {
                    mensajes_leidos += notificacion.id_mensaje + ",";
                    editor.putString("mensajes_leidos", mensajes_leidos);
                    editor.commit();
                    holder.mensaje.setBackgroundColor(getContext().getResources().getColor(R.color.colorSinLeer));
                }
            }
        }
        return convertView;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return listaNotificaciones.size();
    }

    @Override
    public DTONotificacion getItem(int position) {
        // TODO Auto-generated method stub
        return listaNotificaciones.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                Bitmap bmp = null;
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                //Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {

            bmImage.setImageBitmap(result);
        }
    }
    static class ViewHolder {
        protected String tipo;//1:Enviado por usuario,2:SECOM
        protected TextView mensaje;
        protected ImageView imagenMensaje;
    }
}
