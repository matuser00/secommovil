package gsecom.secommovil;

/**
 * Created by tomas on 13/11/16.
 */

public interface OnRefreshListener {
    public void onRefresh();
}
