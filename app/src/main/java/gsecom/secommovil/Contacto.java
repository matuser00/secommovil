package gsecom.secommovil;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class Contacto extends Fragment implements BACKRequestListener{
    View vista=null;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_IMAGE_PICK=2;
    Uri photoURI=null;
    String idUsuario=null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vista=inflater.inflate(R.layout.activity_contacto,container,false);
        SharedPreferences prefs = getContext().getSharedPreferences(getString(R.string.nombresp), Context.MODE_PRIVATE);
        idUsuario=prefs.getString(getString(R.string.keyusuario),"");

        //Mostrar mensaje de notificacion si es que hay alguno:
        String mensajenotificacion=prefs.getString("mensajenotificacion","");
        if(mensajenotificacion!=""){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
            alertDialogBuilder.setTitle("Mensaje de notificación");
            alertDialogBuilder
                    .setMessage(
                            mensajenotificacion
                    );
            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();
            // show it
            alertDialog.show();
            //Remover el mensaje:
            SharedPreferences.Editor editor = prefs.edit();
            editor.remove("mensajenotificacion");
            editor.commit();
        }

        ((TextView)vista.findViewById(R.id.contacto_textoMensaje)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean respuesta=((TextView)vista.findViewById(R.id.contacto_textoMensaje)).getText().toString().trim().length()>0;
                ((Button)vista.findViewById(R.id.contacto_enviarComprobante)).setEnabled(respuesta);
            }
        });

        ((ImageButton)vista.findViewById(R.id.contacto_elegirFotoComprobante)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
                photoPickerIntent.setType("image/*");
                startActivityForResult(Intent.createChooser(photoPickerIntent, "Select Picture"), REQUEST_IMAGE_PICK);
            }
        });
        ((ImageButton)vista.findViewById(R.id.contacto_tomarFotoComprobante)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int permissionCheck= ContextCompat.checkSelfPermission(Contacto.this.getContext(),
                        Manifest.permission.CAMERA);

                if(permissionCheck==-1) {
                    Toast.makeText(getActivity(), "Asigna permisos para utilizar la cámara, desde configuraciones.",
                            Toast.LENGTH_LONG).show();
                }else {
                    // Create an image file name
                    String pathPictures = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/gsecom_pictures/";
                    File filePictures = new File(pathPictures);
                    // Handle the camera action
                    filePictures.mkdirs();
                    //Utils utils = new Utils();
                    String nombreFoto = pathPictures + "img_"+(new Date()).getTime() + ".jpg";
                    File photo = new File(nombreFoto);
                    try {
                        photo.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Uri uri = Uri.fromFile(photo);
                    photoURI=uri;
                    //Abre la camara para tomar foto
                    Intent camIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    //Guardar Imagen
                    camIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    //Retornar a la actividad
                    startActivityForResult(camIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        });
        ((Button)vista.findViewById(R.id.contacto_enviarComprobante)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fotoComprobante!=null) {
                    int permissionCheck = ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.WRITE_CALENDAR);
                    if (permissionCheck == PackageManager.PERMISSION_DENIED)
                        if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                                Manifest.permission.READ_EXTERNAL_STORAGE))
                            requestPermissions(
                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    1);
                }else{
                    //Enviar mensaje:
                    loading = ProgressDialog.show(getActivity(), "Enviando mensaje", "Espere un momento...",true,true);
                    BACKRequestJob requestSubirImagen=new BACKRequestJob(getActivity(),getString(R.string.dirsubirimagen));
                    requestSubirImagen.addBACKRequestListener(Contacto.this);
                    requestSubirImagen.operacion=BACKRequestJob.OPERACION_SUBIRARCHIVO;
                    requestSubirImagen.archivoASubir=fotoComprobante;
                    requestSubirImagen.metodo="POST";
                    String mensajeTexto=((EditText)vista.findViewById(R.id.contacto_textoMensaje)).getText().toString();
                    requestSubirImagen.execute(mensajeTexto,idUsuario);//[0]:mensaje de texto,[1] idusuario
                    ((Button)vista.findViewById(R.id.contacto_enviarComprobante)).setEnabled(false);
                }
            }
        });
        return vista;
    }
    public Uri fotoComprobante=null;
    ProgressDialog loading;
    String nombreTemporal="secom_infocliente_comprobante.jpg";
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            if(resultCode==Activity.RESULT_OK) {
                fotoComprobante=photoURI;
                ((ImageButton)vista.findViewById(R.id.contacto_tomarFotoComprobante)).setImageURI(photoURI);
                ((Button)vista.findViewById(R.id.contacto_enviarComprobante)).setEnabled(true);
            }else{
                ((Button)vista.findViewById(R.id.contacto_enviarComprobante)).setEnabled(false);
            }
        }
        if(requestCode==REQUEST_IMAGE_PICK){
            if(resultCode==Activity.RESULT_OK) {
                photoURI = Uri.parse(data.getDataString());
                fotoComprobante=photoURI;
                ((ImageButton) vista.findViewById(R.id.contacto_elegirFotoComprobante)).setImageURI(photoURI);
                ((Button) vista.findViewById(R.id.contacto_enviarComprobante)).setEnabled(true);

            }else {
                ((Button) vista.findViewById(R.id.contacto_enviarComprobante)).setEnabled(false);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //Iniciar la subida del archivo
            //Subir archivo:
            if(photoURI!=null)
                fotoComprobante=photoURI;
            loading = ProgressDialog.show(getActivity(), "Enviando mensaje", "Espere un momento...",true,true);
            BACKRequestJob requestSubirImagen=new BACKRequestJob(getActivity(),getString(R.string.dirsubirimagen));
            requestSubirImagen.addBACKRequestListener(Contacto.this);
            requestSubirImagen.operacion=BACKRequestJob.OPERACION_SUBIRARCHIVO;
            requestSubirImagen.archivoASubir=fotoComprobante;
            requestSubirImagen.metodo="POST";
            String mensajeTexto=((EditText)vista.findViewById(R.id.contacto_textoMensaje)).getText().toString();
            requestSubirImagen.execute(mensajeTexto,idUsuario);//[0]:mensaje de texto,[1] idusuario
            ((Button)vista.findViewById(R.id.contacto_enviarComprobante)).setEnabled(false);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onExito(BACKRequestEvent e) {
        Toast.makeText(getActivity(), "Mensaje enviado.",
                Toast.LENGTH_LONG).show();
        ((Button)vista.findViewById(R.id.contacto_enviarComprobante)).setEnabled(true);
        ((EditText)vista.findViewById(R.id.contacto_textoMensaje)).setText("");
        ((ImageButton) vista.findViewById(R.id.contacto_elegirFotoComprobante)).setImageDrawable(getActivity().getDrawable(R.drawable.ic_menu_camera));
        ((ImageButton) vista.findViewById(R.id.contacto_tomarFotoComprobante)).setImageDrawable(getActivity().getDrawable(R.drawable.ic_menu_gallery));
        photoURI=null;
        loading.dismiss();
    }

    @Override
    public void onError(BACKRequestEvent e) {
        Toast.makeText(getActivity(), "Error al enviar el mensaje... intentalo de nuevo.",
                Toast.LENGTH_LONG).show();
        ((Button)vista.findViewById(R.id.contacto_enviarComprobante)).setEnabled(true);
        loading.dismiss();
    }
}
