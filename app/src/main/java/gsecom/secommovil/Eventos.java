package gsecom.secommovil;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;

//TODO: Verificar que hacer con la parte de eventos
public class Eventos extends Fragment implements BACKRequestListener, OnRefreshListener {
    View vista=null;
    BACKRequestJob requestEventos=null;
    String idUsuario="";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        vista=inflater.inflate(R.layout.activity_eventos,container,false);
        SharedPreferences prefs = getContext().getSharedPreferences(getString(R.string.nombresp),Context.MODE_PRIVATE);
        idUsuario=prefs.getString(getString(R.string.keyusuario),"");
        cargarEventos();
        return vista;
    }

    public void cargarEventos(){
        ((TextView)vista.findViewById(R.id.eventos_obdatos_texto)).setText("Obteniendo datos...");
        vista.findViewById(R.id.eventos_obdatos_reintentar).setVisibility(View.INVISIBLE);
        vista.findViewById(R.id.eventos_obteniendodatos).setVisibility(View.VISIBLE);
        vista.findViewById(R.id.eventos_listaeventos).setVisibility(View.GONE);
        requestEventos=new BACKRequestJob(getActivity(),getString(R.string.dirhistorialpagos)+idUsuario);
        requestEventos.addBACKRequestListener(this);
        requestEventos.execute("");
    }
    @Override
    public void onExito(BACKRequestEvent e) {
        ((TextView)vista.findViewById(R.id.eventos_obdatos_texto)).setText("");
        vista.findViewById(R.id.eventos_obdatos_reintentar).setVisibility(View.INVISIBLE);
        vista.findViewById(R.id.eventos_obteniendodatos).setVisibility(View.INVISIBLE);
        vista.findViewById(R.id.eventos_listaeventos).setVisibility(View.VISIBLE);
        if(e.respuestaJSONArray.length()>0) {
            ListView iListaEventos=((ListView)vista.findViewById(R.id.eventos_listaeventos));
            ArrayAdapter<DTOEvento> aaEventos = new ArrayAdapter<DTOEvento>(getActivity().getApplicationContext(), R.layout.historiallistapagos_item, R.id.tvitemlistapagos);
            iListaEventos.setAdapter(aaEventos);
            for (int i = 0; i < e.respuestaJSONArray.length(); i++) {
                try {
                    //TODO: Cargar campos que llegan desde el json de eventos
                    DTOEvento evento = new DTOEvento();
                    evento.texto = e.respuestaJSONArray.getJSONObject(i).getString("texto");
                    evento.fecha = e.respuestaJSONArray.getJSONObject(i).getString("fecha");
                    aaEventos.add(evento);
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onError(BACKRequestEvent e) {
        ((TextView)getView().findViewById(R.id.eventos_obdatos_texto)).setText(e.mensaje);
        getView().findViewById(R.id.eventos_obdatos_reintentar).setVisibility(View.VISIBLE);
        getView().findViewById(R.id.eventos_listaeventos).setVisibility(View.GONE);
        getView().findViewById(R.id.eventos_obteniendodatos).setVisibility(View.VISIBLE);
    }

    @Override
    public void onRefresh() {
        cargarEventos();
    }
}
