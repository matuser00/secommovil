package gsecom.secommovil;

/**
 * Created by MAT17 on 15/12/2016.
 */

public class DTONotificacion {
    public String mensaje="";
    public String fecha;
    public String imagen="";
    public String idUsuario;
    public String vistonovisto;
    public String tipo;//1:Mensaje mandado, 2:Respuesta
    public String id_mensaje="";


    public String toString(){
        String tituloItemPago=mensaje+(imagen!=""?"- Imagen adjunta -":"");
        return tituloItemPago;
    }
}
