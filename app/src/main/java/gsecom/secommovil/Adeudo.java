package gsecom.secommovil;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import gsecom.secommovil.R.*;


public class Adeudo extends Fragment implements BACKRequestListener,OnRefreshListener{
    View vista;
    BACKRequestJob requestAdeudo;
    String idAdeudo;
    String idUsuario="";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vista=inflater.inflate(R.layout.activity_adeudo,container,false);
        SharedPreferences prefs = getContext().getSharedPreferences(getString(string.nombresp),Context.MODE_PRIVATE);
        idUsuario=prefs.getString(getString(R.string.keyusuario),"");


        requestAdeudo=new BACKRequestJob(getActivity(), getString(string.diradeudo)+idUsuario);
        requestAdeudo.metodo=BACKRequestJob.METODO_GET;
        requestAdeudo.addBACKRequestListener(this);
        requestAdeudo.execute("");

        ((Button)vista.findViewById(R.id.adeudo_obdatos_reintentar)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((TextView)vista.findViewById(id.adeudo_obdatos_texto)).setText("Obteniendo datos...");
                requestAdeudo.cancel(true);
                if(requestAdeudo.isCancelled()){
                    requestAdeudo=new BACKRequestJob(getActivity(),getString(string.diradeudo)+idUsuario);
                    requestAdeudo.addBACKRequestListener(Adeudo.this);
                    requestAdeudo.metodo=BACKRequestJob.METODO_GET;
                    requestAdeudo.execute("");
                    vista.findViewById(id.adeudo_obdatos_reintentar).setVisibility(View.INVISIBLE);
                }
            }
        });

        ((Button)vista.findViewById(id.adeudo_descargarPDF)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(string.dirrecibopdf)+idAdeudo)));
            }
        });
        return vista;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Snackbar mySnackbar = Snackbar.make(vista,"Mensaje...", 1000);
        mySnackbar.show();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onExito(BACKRequestEvent e) {
        try {
            if(e.respuestaJSONArray!=null) {
                if (e.respuestaJSONArray.length() > 0) {
                    idAdeudo = e.respuestaJSONArray.getJSONObject(0).getString("id_cobro");
                    vista.findViewById(id.adeudo_obteniendodatos).setVisibility(View.INVISIBLE);
                    vista.findViewById(id.adeudo_conInfo).setVisibility(View.VISIBLE);
                    //Periodo de consumo
                    String desde = e.respuestaJSONArray.getJSONObject(0).getJSONObject("lectura_anterior").getString("created_at").substring(0, 10);
                    String hasta = e.respuestaJSONArray.getJSONObject(0).getJSONObject("lectura_actual").getString("created_at").substring(0, 10);
                    ((TextView) vista.findViewById(id.iPeriodo)).setText(desde + " a " + hasta);
                    //DD-MM-AA a DD-MM-AA
                    //Lectura actual:
                    setFlotanteTextView(id.iLecturaActual, e.respuestaJSONArray.getJSONObject(0).getJSONObject("lectura_actual"), "lectura", 3);
                    setBitmapBotonImagen(id.iImgLecturaActual, e.respuestaJSONArray.getJSONObject(0).getJSONObject("lectura_actual"), "foto_lectura");
                    //Lectura anterior:
                    setFlotanteTextView(id.iLecturaAnterior, e.respuestaJSONArray.getJSONObject(0).getJSONObject("lectura_anterior"), "lectura", 3);
                    setBitmapBotonImagen(id.iImgLecturaAnterior, e.respuestaJSONArray.getJSONObject(0).getJSONObject("lectura_anterior"), "foto_lectura");
                    //Metros cúbicos:
                    setFlotanteTextView(id.iMetrosCubicos, e.respuestaJSONArray.getJSONObject(0), "metros_cubicos", 2);
                    //Factor de conv.:
                    setNumeroTextView(id.iFactor, e.respuestaJSONArray.getJSONObject(0), "factor_de_conversion");
                    //Litros:
                    setFlotanteTextView(id.iLitros, e.respuestaJSONArray.getJSONObject(0), "litros", 2);
                    //Precio por litro:
                    setFlotanteTextView(id.iPrecioLitro, e.respuestaJSONArray.getJSONObject(0), "precio_litro", 2);
                    String fechas = e.respuestaJSONArray.getJSONObject(0).getString("fechas");
                    if (fechas.equals("1")) {
                        ((TableRow) vista.findViewById(id.simpleRPrecioLitro)).setVisibility(View.GONE);
                        //Adaptación para el nuevo esquema:
                        //1 ------------------
                        //FechaLimitePago2
                        ((TextView) vista.findViewById(id.tPrecioLitro1)).setText("Precio por litro hasta el " + obtenerFechaDDMMYYYY(e.respuestaJSONArray.getJSONObject(0).getString("fecha_limite_pago2"), false) + ":");
                        setFlotanteTextView(id.iPrecioLitro1, e.respuestaJSONArray.getJSONObject(0), "precio_litro1", 2);
                        setFlotanteTextView(id.iTotalR1, e.respuestaJSONArray.getJSONObject(0), "total1", 2);
                        ((TableRow) vista.findViewById(id.rPrecioLitro1)).setVisibility(View.VISIBLE);
                        //2 ------------------
                        //FechaLimitePago2+1 día - FechaLimitePago3
                        ((TextView) vista.findViewById(id.tPrecioLitro2)).setText("Precio por litro del " + obtenerFechaDDMMYYYY(e.respuestaJSONArray.getJSONObject(0).getString("fecha_limite_pago2"), true) + " al " + obtenerFechaDDMMYYYY(e.respuestaJSONArray.getJSONObject(0).getString("fecha_limite_pago3"), false) + ":");
                        setFlotanteTextView(id.iPrecioLitro2, e.respuestaJSONArray.getJSONObject(0), "precio_litro2", 2);
                        setFlotanteTextView(id.iTotalR2, e.respuestaJSONArray.getJSONObject(0), "total2", 2);
                        ((TableRow) vista.findViewById(id.rPrecioLitro2)).setVisibility(View.VISIBLE);
                        //3 ------------------
                        //FechaLimitePago3
                        ((TextView) vista.findViewById(id.tPrecioLitro3)).setText("Precio por litro despúes del " + obtenerFechaDDMMYYYY(e.respuestaJSONArray.getJSONObject(0).getString("fecha_limite_pago3"), false) + ":");
                        setFlotanteTextView(id.iPrecioLitro3, e.respuestaJSONArray.getJSONObject(0), "precio_litro3", 2);
                        setFlotanteTextView(id.iTotalR3, e.respuestaJSONArray.getJSONObject(0), "total3", 2);
                        ((TableRow) vista.findViewById(id.rPrecioLitro3)).setVisibility(View.VISIBLE);
                    } else {
                        ((TableRow) vista.findViewById(id.simpleRPrecioLitro)).setVisibility(View.VISIBLE);
                        ((TableRow) vista.findViewById(id.rPrecioLitro3)).setVisibility(View.GONE);
                        ((TableRow) vista.findViewById(id.rPrecioLitro2)).setVisibility(View.GONE);
                        ((TableRow) vista.findViewById(id.rPrecioLitro1)).setVisibility(View.GONE);
                    }
                    //Consumo:
                    //setFlotanteTextView(R.id.iConsumo, e.respuestaJSONArray.getJSONObject(0), "consumo", 2);
                    //Recargos, se cambio por adeudo anterior:
                    setFlotanteTextView(id.iRecargos, e.respuestaJSONArray.getJSONObject(0), "adeudo_anterior", 2);
                    //Reparaciones:
                    setFlotanteTextView(id.iReparaciones, e.respuestaJSONArray.getJSONObject(0), "recargos", 2);
                    //Reconexion:
                    setFlotanteTextView(id.iReconexion, e.respuestaJSONArray.getJSONObject(0), "reconexion", 2);
                    //Fecha limite de pago:
                    setTextoTextView(id.iFechaLimitePago, e.respuestaJSONArray.getJSONObject(0), "fecha_limite_pago");
                    DateFormat sourceFormat = new SimpleDateFormat("dd-MM-yyyy");
                    //Adeudo total:
                    Date fechaActual = new Date();
                    Date fecha1 = new Date(), fecha2 = new Date(), fecha3 = new Date(),fecha2a=new Date();

                    try {
                        fecha1 = sourceFormat.parse(obtenerFechaDDMMYYYY(e.respuestaJSONArray.getJSONObject(0).getString("fecha_limite_pago1"), false));
                        fecha2 = sourceFormat.parse(obtenerFechaDDMMYYYY(e.respuestaJSONArray.getJSONObject(0).getString("fecha_limite_pago2"), false));
                        fecha2a = sourceFormat.parse(obtenerFechaDDMMYYYY(e.respuestaJSONArray.getJSONObject(0).getString("fecha_limite_pago2"), true));
                        fecha3 = sourceFormat.parse(obtenerFechaDDMMYYYY(e.respuestaJSONArray.getJSONObject(0).getString("fecha_limite_pago3"), false));
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }

                    String totalDefinido = e.respuestaJSONArray.getJSONObject(0).getString("total");
                    String total=e.respuestaJSONArray.getJSONObject(0).getString("total");
                    String total1=e.respuestaJSONArray.getJSONObject(0).getString("total1");
                    String total2=e.respuestaJSONArray.getJSONObject(0).getString("total2");
                    String total3=e.respuestaJSONArray.getJSONObject(0).getString("total3");
                    if(fechas.equals("1")){
                        //Precio por litro hasta la fecha2
                        if(fechaActual.getMonth()<=fecha2.getMonth()&&fechaActual.getDate()<=fecha2.getDate()&&fechaActual.getYear()==fecha2.getYear()) {
                            totalDefinido = e.respuestaJSONArray.getJSONObject(0).getString("total1");
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                ((TextView) vista.findViewById(id.iPrecioLitro1)).setTextAppearance(style.indicadorPago);
                                ((TextView) vista.findViewById(id.iTotalR1)).setTextAppearance(style.indicadorPago);
                                ((TextView) vista.findViewById(id.tPrecioLitro1)).setTextAppearance(style.indicadorPago);
                            }else{
                                ((TextView)vista.findViewById(id.iPrecioLitro1)).setTextColor(getContext().getResources().getColor(color.colorPrimaryDark));
                                ((TextView)vista.findViewById(id.iPrecioLitro1)).setTypeface(null, Typeface.BOLD);
                                ((TextView)vista.findViewById(id.iTotalR1)).setTextColor(getContext().getResources().getColor(color.colorPrimaryDark));
                                ((TextView)vista.findViewById(id.iTotalR1)).setTypeface(null, Typeface.BOLD);
                                ((TextView)vista.findViewById(id.tPrecioLitro1)).setTextColor(getContext().getResources().getColor(color.colorPrimaryDark));
                                ((TextView)vista.findViewById(id.tPrecioLitro1)).setTypeface(null, Typeface.BOLD);
                            }
                        }
                        //Desde fecha2+1Dia hasta fecha3
                        if((fechaActual.getMonth()>=fecha2a.getMonth()&&fechaActual.getDate()>=fecha2a.getDate()&&fechaActual.getYear()==fecha2a.getYear())&&(fechaActual.getMonth()<=fecha3.getMonth()&&fechaActual.getDate()<=fecha3.getDate()&&fechaActual.getYear()<=fecha3.getYear())) {
                            totalDefinido = e.respuestaJSONArray.getJSONObject(0).getString("total2");
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                ((TextView)vista.findViewById(id.iPrecioLitro2)).setTextAppearance(style.indicadorPago);
                                ((TextView)vista.findViewById(id.iTotalR2)).setTextAppearance(style.indicadorPago);
                                ((TextView)vista.findViewById(id.tPrecioLitro2)).setTextAppearance(style.indicadorPago);
                            }else{
                                ((TextView)vista.findViewById(id.iPrecioLitro2)).setTextColor(getContext().getResources().getColor(color.colorPrimaryDark));
                                ((TextView)vista.findViewById(id.iPrecioLitro2)).setTypeface(null, Typeface.BOLD);
                                ((TextView)vista.findViewById(id.iTotalR2)).setTextColor(getContext().getResources().getColor(color.colorPrimaryDark));
                                ((TextView)vista.findViewById(id.iTotalR2)).setTypeface(null, Typeface.BOLD);
                                ((TextView)vista.findViewById(id.tPrecioLitro2)).setTextColor(getContext().getResources().getColor(color.colorPrimaryDark));
                                ((TextView)vista.findViewById(id.tPrecioLitro2)).setTypeface(null, Typeface.BOLD);
                            }
                        }
                        //Despues de la fecha3
                        if(fechaActual.getMonth()>=fecha3.getMonth()) {
                            boolean asignar3=false;
                            if(fechaActual.getYear()==fecha3.getYear()){
                                if(fechaActual.getMonth()>fecha3.getMonth()) {
                                    asignar3=true;
                                }
                                if(fechaActual.getMonth()==fecha3.getMonth()){
                                    if(fechaActual.getDate()>fecha3.getDate()){
                                        asignar3=true;
                                    }
                                }
                            }
                            if(asignar3) {
                                totalDefinido = e.respuestaJSONArray.getJSONObject(0).getString("total3");
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    ((TextView) vista.findViewById(id.iPrecioLitro3)).setTextAppearance(style.indicadorPago);
                                    ((TextView) vista.findViewById(id.iTotalR3)).setTextAppearance(style.indicadorPago);
                                    ((TextView) vista.findViewById(id.tPrecioLitro3)).setTextAppearance(style.indicadorPago);
                                }else{
                                    ((TextView) vista.findViewById(id.iPrecioLitro3)).setTextColor(getContext().getResources().getColor(color.colorPrimaryDark));
                                    ((TextView)vista.findViewById(id.iPrecioLitro3)).setTypeface(null, Typeface.BOLD);
                                    ((TextView) vista.findViewById(id.iTotalR3)).setTextColor(getContext().getResources().getColor(color.colorPrimaryDark));
                                    ((TextView)vista.findViewById(id.iTotalR3)).setTypeface(null, Typeface.BOLD);
                                    ((TextView) vista.findViewById(id.tPrecioLitro3)).setTextColor(getContext().getResources().getColor(color.colorPrimaryDark));
                                    ((TextView)vista.findViewById(id.tPrecioLitro3)).setTypeface(null, Typeface.BOLD);
                                }
                            }
                        }
                        if(total1!=total&&total2!=total&&total3!=total){
                            totalDefinido=total;
                        }
                    }

                    ((TextView)vista.findViewById(id.adeudo_total)).setText(totalDefinido);

                } else {
                    getView().findViewById(id.adeudo_sinAdeudo).setVisibility(View.VISIBLE);
                    getView().findViewById(id.adeudo_obteniendodatos).setVisibility(View.INVISIBLE);
                }
            }
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
    }

    private String obtenerFechaDDMMYYYY(String fecha,boolean incrementardia){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date dateflp2= null;
        try {
            dateflp2 = formatter.parse(fecha+" 00:00:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(incrementardia)
            dateflp2=new Date(dateflp2.getTime()+(1000 * 60 * 60 * 24));
        SimpleDateFormat formatter2= new SimpleDateFormat("dd-MM-yyyy");
        return formatter2.format(dateflp2);
    }

    private void setBitmapBotonImagen(int idImagen,JSONObject jo, String nombrePropiedad){
        final int iIdImagen=idImagen;
        BACKRequestJob requestImagen= null;
        try {
            requestImagen = new BACKRequestJob(getActivity(), FuncionesGenerales.BASE_URL_GSECOM+"public/"+jo.getString(nombrePropiedad));
            requestImagen.metodo=BACKRequestJob.METODO_GET;
            requestImagen.addBACKRequestListener(new BACKRequestListener() {

                public void onExito(final BACKRequestEvent e) {
                    ((ImageButton) vista.findViewById(iIdImagen)).setImageBitmap(e.bitmapImagen);
                    ((ImageButton)vista.findViewById(iIdImagen)).setOnClickListener(new View.OnClickListener() {
                        final String src=e.direccion;
                        @Override
                        public void onClick(View v) {
                            Intent intento= new Intent(getActivity().getApplicationContext(),ImagenMaximizada.class);
                            intento.putExtra("src",src);
                            startActivity(intento);
                        }
                    });
                }

                @Override
                public void onError(BACKRequestEvent e) {

                }
            });
            requestImagen.execute("");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setNumeroTextView(int idTextView, JSONObject jo, String nombrePropiedad){
        try {
            ((TextView)vista.findViewById(idTextView)).setText(""+(jo.getInt(nombrePropiedad)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void setFlotanteTextView(int idTextView, JSONObject jo,String nombrePropiedad,int nDecimales){
        try {
            ((TextView)getView().findViewById(idTextView)).setText((nombrePropiedad=="precio_litro"||nombrePropiedad=="adeudo_anterior"||nombrePropiedad=="reconexion"||nombrePropiedad=="reparaciones"||nombrePropiedad=="consumo"?"$":"")+ String.format("%."+nDecimales+"f",jo.getDouble(nombrePropiedad)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void setTextoTextView(int idTextView, JSONObject jo, String nombrePropiedad){
        try {
            ((TextView)vista.findViewById(idTextView)).setText(jo.getString(nombrePropiedad));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onError(BACKRequestEvent e) {
        ((TextView)vista.findViewById(id.adeudo_obdatos_texto)).setText(e.mensaje);
        vista.findViewById(id.adeudo_obdatos_reintentar).setVisibility(View.VISIBLE);
        vista.findViewById(R.id.adeudo_sinAdeudo).setVisibility(View.GONE);
        requestAdeudo.cancel(true);
    }

    @Override
    public void onRefresh() {
        ((TextView)vista.findViewById(id.adeudo_obdatos_texto)).setText("Obteniendo datos...");
        vista.findViewById(id.adeudo_obdatos_reintentar).setVisibility(View.INVISIBLE);
        vista.findViewById(id.adeudo_obteniendodatos).setVisibility(View.VISIBLE);
        vista.findViewById(id.adeudo_conInfo).setVisibility(View.GONE);
        getView().findViewById(id.adeudo_sinAdeudo).setVisibility(View.GONE);
        requestAdeudo=new BACKRequestJob(getActivity(),getString(string.diradeudo)+idUsuario);
        requestAdeudo.addBACKRequestListener(Adeudo.this);
        requestAdeudo.metodo=BACKRequestJob.METODO_GET;
        requestAdeudo.execute("");
    }
}
