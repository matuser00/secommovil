package gsecom.secommovil;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.OverScroller;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
//TODO: Verificar como mejorar la manera de hacer zoom en la imagen etc...
public class ImagenMaximizada extends AppCompatActivity {
    ImageView iImagenMax=null;
     public OverScroller overScroller=null;
    GestureDetectorCompat gestureDetectorCompat=null;
    private ScaleGestureDetector mScaleDetector;
    private float mScaleFactor = 1.f;

    public int positionX=0;
    public int positionY=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_imagen_maximizada);

        BACKRequestJob requestImagen= null;
        requestImagen = new BACKRequestJob(this, getIntent().getExtras().getString("src"));
        requestImagen.addBACKRequestListener(new BACKRequestListener() {
            @Override
            public void onExito(final BACKRequestEvent e) {
                iImagenMax=((ImageView)findViewById(R.id.iImagenMax));
                iImagenMax.setImageBitmap(e.bitmapImagen);
                iImagenMax.setMinimumWidth(iImagenMax.getWidth());
                iImagenMax.setMinimumHeight(iImagenMax.getHeight());
                mScaleDetector = new ScaleGestureDetector(getApplicationContext(), new ScaleListener());
            }

            @Override
            public void onError(BACKRequestEvent e) {
                Intent ultimaActividad=new Intent();
                ultimaActividad.putExtra("mensajeError",e.mensaje);
                setResult(Activity.RESULT_OK,ultimaActividad);
                finish();
            }
        });
        requestImagen.execute("");
        overScroller= new OverScroller(this.getBaseContext());
        gestureDetectorCompat=new GestureDetectorCompat(getApplicationContext(),new GestureDetector.SimpleOnGestureListener(){
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
                                    float distanceY) {
                if(((int)iImagenMax.getWidth()*mScaleFactor)>((FrameLayout)findViewById(R.id.iConiImagenMax)).getWidth()) {
                    float deltaY = e2.getY() - e1.getY();
                    float deltaX = e2.getX() - e1.getX();
                    float SLIDE_THRESHOLD = 250;
                    int parametroScroll=10;
                    if (Math.abs(deltaX) > Math.abs(deltaY)) {
                        if (Math.abs(deltaX) > SLIDE_THRESHOLD) {
                            if (deltaX > 0) {
                                // the user made a sliding right gesture
                                if(iImagenMax.getScrollX()-parametroScroll>=((int)iImagenMax.getWidth()/2*-1))
                                    iImagenMax.scrollBy(parametroScroll * -1, 0);
                            } else {
                                // the user made a sliding left gesture
                                if(iImagenMax.getScrollX()+parametroScroll<=((int)iImagenMax.getWidth()/2))
                                    iImagenMax.scrollBy(parametroScroll, 0);
                            }
                        }
                    } else {
                        if (Math.abs(deltaY) > SLIDE_THRESHOLD) {
                            if (deltaY > 0) {
                                // the user made a sliding down gesture
                                if(iImagenMax.getScrollY()-parametroScroll>=((int)iImagenMax.getHeight()/2*-1))
                                    iImagenMax.scrollBy(0, parametroScroll * -1);
                            } else {
                                // the user made a sliding up gesture
                                if(iImagenMax.getScrollX()+parametroScroll<=((int)iImagenMax.getHeight()/2))
                                    iImagenMax.scrollBy(0, parametroScroll);
                            }
                        }
                    }
                }
                return true;
            }
        });

    }


    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        // Let the ScaleGestureDetector inspect all events.
        mScaleDetector.onTouchEvent(ev);
        gestureDetectorCompat.onTouchEvent(ev);

        return true;
    }

    private void zoom(){
        mScaleFactor=mScaleFactor>=1.0f?mScaleFactor:1.0f;
        iImagenMax.setScaleX(mScaleFactor);
        iImagenMax.setScaleY(mScaleFactor);
        if(((int)iImagenMax.getWidth()*mScaleFactor)<=((FrameLayout)findViewById(R.id.iConiImagenMax)).getWidth())
            iImagenMax.scrollTo(0,0);

    }

    private class ScaleListener
            extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            mScaleFactor *= detector.getScaleFactor();
            // Don't let the object get too small or too large.
            mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 5.0f));
            zoom();
            return true;
        }
    }
}

