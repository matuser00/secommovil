package gsecom.secommovil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Pago extends Activity implements BACKRequestListener{
    BACKRequestJob requestPago=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pago);
        requestPago= new BACKRequestJob(this,getString(R.string.dirpago)+getIntent().getExtras().getInt("pago"));
        requestPago.addBACKRequestListener(this);
        requestPago.execute("");
        ((Button)findViewById(R.id.pago_obdatos_reintentar)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((TextView)findViewById(R.id.pago_obdatos_texto)).setText("Obteniendo datos...");
                requestPago.cancel(true);
                if(requestPago.isCancelled()){
                    requestPago= new BACKRequestJob(Pago.this,getString(R.string.dirpago)+getIntent().getExtras().getInt("pago"));
                    requestPago.addBACKRequestListener(Pago.this);
                    requestPago.execute("");
                    findViewById(R.id.pago_obdatos_reintentar).setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    private String obtenerFechaDDMMYYYY(String fecha,boolean incrementardia){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date dateflp2= null;
        try {
            dateflp2 = formatter.parse(fecha+" 00:00:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(incrementardia)
            dateflp2=new Date(dateflp2.getTime()+(1000 * 60 * 60 * 24));
        SimpleDateFormat formatter2= new SimpleDateFormat("dd-MM-yyyy");
        return formatter2.format(dateflp2);
    }

    @Override
    public void onExito(BACKRequestEvent e) {
        //Periodo de consumo
        try {
            String hasta="";
            JSONObject jo=e.respuestaJSON.getJSONObject("lectura_actual");
            if(jo!=null)
                    hasta=e.respuestaJSON.getJSONObject("lectura_actual").getString("created_at").substring(0,10);
            String desde=e.respuestaJSON.getJSONObject("lectura_anterior").getString("created_at").substring(0,10);
            ((TextView)findViewById(R.id.pago_periodo)).setText(desde+" a "+hasta);
            //Lectura actual
            setFlotanteTextView(R.id.pago_lecturaactual,e.respuestaJSON.getJSONObject("lectura_actual"),"lectura",3);
            setBitmapBotonImagen(R.id.pago_iimglecturaactual,e.respuestaJSON.getJSONObject("lectura_actual"),"foto_lectura");
            //Lectura anterior
            setFlotanteTextView(R.id.pago_lecturaanterior,e.respuestaJSON.getJSONObject("lectura_anterior"),"lectura",3);
            setBitmapBotonImagen(R.id.pago_iimglecturaanterior,e.respuestaJSON.getJSONObject("lectura_anterior"),"foto_lectura");
            //Metros cúbicos
            setFlotanteTextView(R.id.pago_metroscubicos,e.respuestaJSON,"metros_cubicos",2);
            //Factor de conv.
            setFlotanteTextView(R.id.pago_factor,e.respuestaJSON,"factor_de_conversion",2);
            //Litros
            setFlotanteTextView(R.id.pago_litros,e.respuestaJSON,"litros",2);
            //Precio por litro
            setFlotanteTextView(R.id.pago_preciolitro,e.respuestaJSON,"precio_litro",2);
            //Consumo
            setFlotanteTextView(R.id.pago_consumo,e.respuestaJSON,"consumo",2);
            //Reparaciones:
            setFlotanteTextView(R.id.pago_reparaciones,e.respuestaJSON,"cargos_extras",2);
            //Recargos, se cambio por adeudo anterior
            setFlotanteTextView(R.id.pago_recargos,e.respuestaJSON,"adeudo_anterior",2);
            //Reparaciones - ?
            //Reconexion
            setFlotanteTextView(R.id.pago_reconexion,e.respuestaJSON,"reconexion",2);
            //Total
            setFlotanteTextView(R.id.pago_total,e.respuestaJSON,"total",2);
            //Pago
            setFlotanteTextView(R.id.pago_pago,e.respuestaJSON,"pago",2);
            //Fecha pago:
            ((TextView)findViewById(R.id.pago_fechapago)).setText(obtenerFechaDDMMYYYY(e.respuestaJSON.getString("fecha_pago"), false));
            //Modo de pago (0/1:Efectivo,2:Bancomer,3:Banamex,4:Banorte,6:Scotiabank):
            String modopago="";
            int int_modopago=e.respuestaJSON.getInt("id_forma_pago");
            if(int_modopago==1||int_modopago==0)
                modopago="Efectivo";
            if(int_modopago==2)
                modopago="Bancomer";
            if(int_modopago==3)
                modopago="Banamex";
            if(int_modopago==4)
                modopago="Banorte";
            if(int_modopago==6)
                modopago="Scotiabank";

            ((TextView)findViewById(R.id.pago_modopago)).setText(modopago);

            findViewById(R.id.pago_info).setVisibility(View.VISIBLE);
            findViewById(R.id.pago_obteniendodatos).setVisibility(View.INVISIBLE);
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public void onError(BACKRequestEvent e) {
        ((TextView)findViewById(R.id.pago_obdatos_texto)).setText(e.mensaje);
        findViewById(R.id.pago_obdatos_reintentar).setVisibility(View.VISIBLE);
    }
    /*Rutinas SET */
    private void setNumeroTextView(int idTextView, JSONObject jo, String nombrePropiedad){
        try {
            ((TextView)findViewById(idTextView)).setText(""+(jo.getInt(nombrePropiedad)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void setFlotanteTextView(int idTextView, JSONObject jo,String nombrePropiedad,int nDecimales){
        try {
            ((TextView)findViewById(idTextView)).setText((nombrePropiedad=="cargos_extras"||nombrePropiedad=="total"||nombrePropiedad=="pago"||nombrePropiedad=="precio_litro"||nombrePropiedad=="adeudo_anterior"||nombrePropiedad=="reconexion"||nombrePropiedad=="consumo"?"$":"")+ String.format("%."+nDecimales+"f",jo.getDouble(nombrePropiedad)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void setTextoTextView(int idTextView, JSONObject jo, String nombrePropiedad){
        try {
            ((TextView)findViewById(idTextView)).setText(jo.getString(nombrePropiedad));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void setBitmapBotonImagen(int idImagen,JSONObject jo, String nombrePropiedad){
        final int iIdImagen=idImagen;
        BACKRequestJob requestImagen= null;
        try {
            requestImagen = new BACKRequestJob(this, getString(R.string.dirbaseback)+jo.getString(nombrePropiedad));
            requestImagen.addBACKRequestListener(new BACKRequestListener() {
                public void onExito(final BACKRequestEvent e) {
                    ((ImageButton)findViewById(iIdImagen)).setImageBitmap(e.bitmapImagen);
                    ((ImageButton)findViewById(iIdImagen)).setOnClickListener(new View.OnClickListener() {
                        final String src=e.direccion;
                        @Override
                        public void onClick(View v) {
                            Intent intento= new Intent(getBaseContext(),ImagenMaximizada.class);
                            intento.putExtra("src",src);
                            startActivity(intento);
                        }
                    });
                }

                @Override
                public void onError(BACKRequestEvent e) {
                    ((TextView)findViewById(R.id.pago_obdatos_texto)).setText(e.mensaje);
                    findViewById(R.id.pago_obdatos_reintentar).setVisibility(View.VISIBLE);
                }
            });
            requestImagen.execute("");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
