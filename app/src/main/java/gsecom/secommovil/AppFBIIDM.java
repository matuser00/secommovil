package gsecom.secommovil;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by MAT17 on 25/01/2017.
 */

public class AppFBIIDM extends FirebaseInstanceIdService {
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onTokenRefresh() {
        SharedPreferences prefs = this.getSharedPreferences(getString(R.string.nombresp), Context.MODE_PRIVATE);
        String idUsuario = prefs.getString(getString(R.string.keyusuario), "");
        BACKRequestRegistroToken.startRegistro(this.getApplicationContext(),idUsuario,FirebaseInstanceId.getInstance().getToken());
    }

}