package gsecom.secommovil;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.json.JSONException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import gsecom.secommovil.R.*;

//TODO: Al dar back en cualquier pantalla regresar al home
//TODO: Actualizacion automática 3 veces al dia en página de inicio
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,BACKRequestListener {
    String idUsuario=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences prefs =getSharedPreferences(getString(string.nombresp),Context.MODE_PRIVATE);
        idUsuario=prefs.getString(getString(string.keyusuario),"");

        if(idUsuario==""||idUsuario==null ) {
            //Dirigir a inicio de sesion:
            String mensajenotificacion=this.getIntent().getStringExtra("mensajenotificacion");
            Intent fromDtoB = new Intent(this,InicioSesion.class);
            if(mensajenotificacion!=""&&mensajenotificacion!=null){
                fromDtoB.putExtra("mensajenotificacion",mensajenotificacion);
            }

            fromDtoB.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(fromDtoB);
        }else{
            setContentView(R.layout.activity_main);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();


            ventanaContacto=this.getIntent().getBooleanExtra("respondernotificacion",false)==true;

            String mensajenotificacion=this.getIntent().getStringExtra("mensajenotificacion");

            if(mensajenotificacion!=""&&mensajenotificacion!=null){
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("mensajenotificacion",mensajenotificacion);
                editor.commit();
                NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                navigationView.setNavigationItemSelectedListener(this);
                getSupportFragmentManager().beginTransaction().replace(R.id.content_main,new Contacto()).commit();
            }else{
                NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                navigationView.setNavigationItemSelectedListener(this);
                getSupportFragmentManager().beginTransaction().replace(R.id.content_main,new Home()).commit();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Usado para cuando se intenta tomar una foto
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            if (getSupportFragmentManager().getFragments() != null) {
                for (Fragment fragmento : getSupportFragmentManager().getFragments()) {
                    if (fragmento != null) {
                        if (fragmento.isVisible()) {
                            if (fragmento instanceof OnRefreshListener)
                                ((OnRefreshListener) fragmento).onRefresh();
                        }
                    }
                }
                return true;
            }
        }

        if(id==R.id.menu_metodopago){
            BACKRequestJob request= new BACKRequestJob(this,getString(string.diradeudo)+idUsuario);
            request.addBACKRequestListener(this);
            request.execute("");
        }

        return super.onOptionsItemSelected(item);
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Fragment fragmentoSeleccionado=null;
        ventanaContacto=false;
        switch(id){
            case R.id.menuNotificaciones:
                Intent intentoNotificaciones= new Intent(this.getApplicationContext(),Notificaciones.class);
                startActivity(intentoNotificaciones);
                break;
            case R.id.menuAdeudo:
                fragmentoSeleccionado=new Adeudo();
                break;
            case R.id.menuHPagos:
                fragmentoSeleccionado=new HistorialPagos();
                break;
            case R.id.menuContacto:
                ventanaContacto=true;
                fragmentoSeleccionado=new Contacto();
                break;
            case R.id.menuHome:
                fragmentoSeleccionado=new Home();
                break;
            case R.id.menuPreguntas:
                Intent intentoPreguntas= new Intent(this.getApplicationContext(),LectorPdfWV.class);
                intentoPreguntas.putExtra("url",getString(string.dirpreguntas));
                startActivity(intentoPreguntas);
                break;
            case R.id.menuSugerencias:
                Intent intentoSugerencias= new Intent(this.getApplicationContext(),LectorPdfWV.class);
                intentoSugerencias.putExtra("url",getString(string.dirsugerencias));
                startActivity(intentoSugerencias);
                break;
            case R.id.menuAvisoPrivacidad:
                Intent intento_lectorpdf= new Intent(this.getApplicationContext(),LectorPdfWV.class);
                intento_lectorpdf.putExtra("url",getString(R.string.dirpdfprivacidad));
                startActivity(intento_lectorpdf);
                break;
            case R.id.menu_cerrarSesion:
                SharedPreferences settings = this.getSharedPreferences(getString(R.string.nombresp), Context.MODE_PRIVATE);
                settings.edit().remove(getString(R.string.keyusuario)).commit();
                Intent fromDtoB = new Intent(this,InicioSesion.class);
                fromDtoB.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(fromDtoB);
                break;
            default:
                break;
        }
        if(fragmentoSeleccionado!=null)
            getSupportFragmentManager().beginTransaction().replace(R.id.content_main,fragmentoSeleccionado).commit();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        Fragment fragmentoSeleccionado=null;
        fragmentoSeleccionado=new Home();
        if(fragmentoSeleccionado!=null)
            getSupportFragmentManager().beginTransaction().replace(R.id.content_main,fragmentoSeleccionado).commit();
    }
    public static boolean ventanaContacto=false;
    @Override
    public void onExito(BACKRequestEvent e) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);
        // set title
        alertDialogBuilder.setTitle("Información de pagos");
        // set dialog message
        try {
            String fechas = e.respuestaJSONArray.getJSONObject(0).getString("fechas");

            DateFormat sourceFormat = new SimpleDateFormat("dd-MM-yyyy");
            //Adeudo total:
            Date fechaActual = new Date();
            Date fecha1 = new Date(), fecha2 = new Date(), fecha3 = new Date(),fecha2a=new Date();

            try {
                fecha1 = sourceFormat.parse(obtenerFechaDDMMYYYY(e.respuestaJSONArray.getJSONObject(0).getString("fecha_limite_pago1"), false));
                fecha2 = sourceFormat.parse(obtenerFechaDDMMYYYY(e.respuestaJSONArray.getJSONObject(0).getString("fecha_limite_pago2"), false));
                fecha2a = sourceFormat.parse(obtenerFechaDDMMYYYY(e.respuestaJSONArray.getJSONObject(0).getString("fecha_limite_pago2"), true));
                fecha3 = sourceFormat.parse(obtenerFechaDDMMYYYY(e.respuestaJSONArray.getJSONObject(0).getString("fecha_limite_pago3"), false));
            } catch (ParseException e1) {
                e1.printStackTrace();
            }

            String totalDefinido = e.respuestaJSONArray.getJSONObject(0).getString("total");
            String total=e.respuestaJSONArray.getJSONObject(0).getString("total");
            String total1=e.respuestaJSONArray.getJSONObject(0).getString("total1");
            String total2=e.respuestaJSONArray.getJSONObject(0).getString("total2");
            String total3=e.respuestaJSONArray.getJSONObject(0).getString("total3");
            if(fechas.equals("1")){
                //Precio por litro hasta la fecha2
                if(fechaActual.getMonth()<=fecha2.getMonth()&&fechaActual.getDate()<=fecha2.getDate()&&fechaActual.getYear()==fecha2.getYear()) {
                    totalDefinido = e.respuestaJSONArray.getJSONObject(0).getString("total1");
                }
                //Desde fecha2+1Dia hasta fecha3
                if((fechaActual.getMonth()>=fecha2a.getMonth()&&fechaActual.getDate()>=fecha2a.getDate()&&fechaActual.getYear()==fecha2a.getYear())&&(fechaActual.getMonth()<=fecha3.getMonth()&&fechaActual.getDate()<=fecha3.getDate()&&fechaActual.getYear()<=fecha3.getYear())) {
                    totalDefinido = e.respuestaJSONArray.getJSONObject(0).getString("total2");
                }
                //Despues de la fecha3
                if(fechaActual.getMonth()>=fecha3.getMonth()) {
                    boolean asignar3=false;
                    if(fechaActual.getYear()==fecha3.getYear()){
                        if(fechaActual.getMonth()>fecha3.getMonth()) {
                            asignar3=true;
                        }
                        if(fechaActual.getMonth()==fecha3.getMonth()){
                            if(fechaActual.getDate()>fecha3.getDate()){
                                asignar3=true;
                            }
                        }
                    }
                    if(asignar3) {
                        totalDefinido = e.respuestaJSONArray.getJSONObject(0).getString("total3");
                    }
                }
                if(total1!=total&&total2!=total&&total3!=total){
                    totalDefinido=total;
                }
            }

            String mensajeinformacionbancaria=MainActivity.this.getString(string.infobancaria);

            SharedPreferences prefs = getSharedPreferences(getString(R.string.nombresp), Context.MODE_PRIVATE);
            String codigocliente=prefs.getString(getString(string.codcliente),"");
            int codscotiabank=prefs.getInt(this.getString(string.codscotiabank),-1);

            mensajeinformacionbancaria=mensajeinformacionbancaria.replaceAll("=referencia=",codigocliente);

            if(codscotiabank!=-1)
                mensajeinformacionbancaria=mensajeinformacionbancaria.replaceAll("=referencia_scotiabank=",codigocliente+""+codscotiabank);

            alertDialogBuilder
                    .setMessage(
                            mensajeinformacionbancaria+"\n ADEUDO ACTUAL: $"+totalDefinido.substring(0,totalDefinido.indexOf('.')+3)
                    );
        } catch (JSONException e1) {
            String mensajeinformacionbancaria=MainActivity.this.getString(string.infobancaria);

            SharedPreferences prefs = getSharedPreferences(getString(R.string.nombresp), Context.MODE_PRIVATE);
            String codigocliente=prefs.getString(getString(string.codcliente),"");

            mensajeinformacionbancaria=mensajeinformacionbancaria.replaceAll("=referencia=",codigocliente);
            alertDialogBuilder
                    .setMessage(
                            mensajeinformacionbancaria
                    );
        }
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

    private String obtenerFechaDDMMYYYY(String fecha,boolean incrementardia){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date dateflp2= null;
        try {
            dateflp2 = formatter.parse(fecha+" 00:00:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(incrementardia)
            dateflp2=new Date(dateflp2.getTime()+(1000 * 60 * 60 * 24));
        SimpleDateFormat formatter2= new SimpleDateFormat("dd-MM-yyyy");
        return formatter2.format(dateflp2);
    }


    @Override
    public void onError(BACKRequestEvent e) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);
        // set title
        alertDialogBuilder.setTitle("Información de pagos");
        // set dialog message
        alertDialogBuilder
                .setMessage(
                        MainActivity.this.getString(R.string.infobancaria)
                );
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }
}