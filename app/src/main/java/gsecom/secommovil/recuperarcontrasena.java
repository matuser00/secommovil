package gsecom.secommovil;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

public class
recuperarcontrasena extends AppCompatActivity implements BACKRequestListener{
    EditText codcliente;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recuperarcontrasena);
        Button btnrecuperar=(Button)findViewById(R.id.reccon_btn_recuperar);
        codcliente=(EditText)findViewById(R.id.reccon_codigoc);

        btnrecuperar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(codcliente.getText().toString().trim().length()>0) {
                    BACKRequestJob requestRecuperar = new BACKRequestJob(recuperarcontrasena.this, getString(R.string.dirrecuperar));
                    requestRecuperar.addBACKRequestListener(recuperarcontrasena.this);
                    requestRecuperar.metodo="POST";
                    requestRecuperar.execute("num_cliente="+((EditText)findViewById(R.id.reccon_codigoc)).getText(),"email="+((EditText)findViewById(R.id.reccon_email)).getText());
                }else{
                    Toast.makeText(recuperarcontrasena.this,"Debe llenar correctamente el campo de código de cliente",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onExito(BACKRequestEvent e) {
        try {
            ((TextView)findViewById(R.id.reccon_nueva)).setText("Está es tu nueva contraseña, por favor anotala: "+e.respuestaJSON.getString("password"));
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public void onError(BACKRequestEvent e) {
        Toast.makeText(this,"No se pudo realizár la solicitud para recuperar contraseña, intenta de nuevo más tarde.",Toast.LENGTH_SHORT).show();
    }
}
