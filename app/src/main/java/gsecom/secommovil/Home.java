package gsecom.secommovil;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class Home extends Fragment {
    View vista;
    //TODO:Implementar notificaciones
    //TODO:Registrar token
    //TODO:Abrir pdf de adeudo despues de descargar
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vista=inflater.inflate(R.layout.fragment_home,container,false);
        ((WebView)vista.findViewById(R.id.home_webView)).setWebViewClient(new WebViewClient()
        {
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                ((WebView)getView().findViewById(R.id.home_webView)).loadUrl(getString(R.string.dirhomelocal));
            }
        });
        ((WebView)vista.findViewById(R.id.home_webView)).loadUrl(getString(R.string.dirhomeweb));
        return vista;
    }
}
